//
//  ObjC.h
//  AuthenteqHelperObjC
//
//  Created by Vitaliy Gozhenko on 6/27/19.
//  Copyright © 2019 Vitaliy Gozhenko. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ObjC : NSObject

+ (BOOL)catchException:(void(^)(void))tryBlock error:(__autoreleasing NSError **)error;

@end

NS_ASSUME_NONNULL_END
