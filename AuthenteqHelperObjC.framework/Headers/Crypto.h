//
//  Crypto.h
//  Authenteq
//
//  Created by Vitaliy Gozhenko on 1/28/18.
//  Copyright © 2018 Authenteq. All rights reserved.
//

#if !defined CRYPTO_H
#define CRYPTO_H

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface Crypto : NSObject

+ (NSData * _Nullable)encryptedDataForData:(NSData *)data
                                  password:(NSString *)password
                                        iv:(NSData *)iv
                                      salt:(NSData *)salt
                                     error:(NSError **)error;

+ (NSData * _Nullable)decryptedDataForData:(NSData *)data
                                  password:(NSString *)password
                                        iv:(NSData *)iv
                                      salt:(NSData *)salt
                                     error:(NSError **)error;

+ (NSData * _Nullable)pbkdf2SHA512:(NSData *)password
                              salt:(NSData *)salt
                      keyByteCount:(NSInteger)keyByteCount
                            rounds:(NSInteger)rounds;

+ (NSData * _Nullable)hmacSHA256ForKey:(NSData *)key data:(NSData *)data;
+ (NSData * _Nullable)sha3Length256:(NSData *)data;
+ (NSData * _Nullable)sha256:(NSData *)data;
+ (NSData * _Nullable)sha512:(NSData *)data;

@end

NS_ASSUME_NONNULL_END

#endif
