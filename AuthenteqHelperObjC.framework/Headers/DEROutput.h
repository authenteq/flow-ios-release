//
//  DerOutput.h
//  Authenteq
//
//  Created by Vitaliy Gozhenko on 1/26/18.
//  Copyright © 2018 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(UInt32, DerTag) {
  DerTagTagged = 0x80,
  DerTagFulfillmentSignature = 0x81,
  DerTagFulfillmentResult = 0xA4,
  DerTagConditionFingerprint = 0x30,
  DerTagConditionCost = 0x81,
  DerTagConditionResult = 0xA4,
  DerTagConditionHash = 0x30
};

#if defined __cplusplus
extern "C" {
#endif

extern unsigned sizeOfUnsignedInt (UInt64 n);
extern unsigned encodeUnsignedInt (UInt64 n, UInt8 buf[], BOOL padHighBit);
extern unsigned encodeSignedInt (SInt64 n, UInt8 buf[]);
  
#if defined __cplusplus
};
#endif 

@interface DerOutput: NSObject

- (void)writeTag:(DerTag)tag data:(NSData *)data;
- (void)writeTag:(DerTag)tag bytes:(const void *)bytes length:(size_t)length;

- (NSData *)result;

@end

