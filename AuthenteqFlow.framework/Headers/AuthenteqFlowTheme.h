//
//  AuthenteqFlowTheme.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AuthenteqFlowTheme : NSObject


@property (nonatomic, readonly, strong) UIColor * primaryColor;
@property (nonatomic, readonly, strong) UIColor * textColor;
@property (nonatomic, readonly, strong) UIColor * screenBackgroundColor;
@property (nonatomic, readonly, strong) UIColor * viewBackgroundHightlightColor;
@property (nonatomic, readonly, strong) UIColor * separatorColor;
@property (nonatomic, readonly, strong) UIColor * overlayColor;
@property (nonatomic, readonly, strong) UIColor * greenColor;
@property (nonatomic, readonly, strong) UIColor * placeholderTextColor;
@property (nonatomic, readonly, strong) UIFont * font;
@property (nonatomic, readonly, strong) UIFont * boldFont;
@property (nonatomic, readonly, strong) UIImage * _Nullable identificationInstructionImageForSelfie;
@property (nonatomic, readonly, strong) UIImage * _Nullable identificationInstructionImageForDriverLicense;
@property (nonatomic, readonly, strong) UIImage * _Nullable identificationInstructionImageForIdCard;
@property (nonatomic, readonly, strong) UIImage * _Nullable identificationInstructionImageForPassport;

/// <em>[deprecated]</em> Theme class for UI customization
/// \param primaryColor Main color of your scheme
///
/// \param textColor Text color
///
/// \param screenBackgroundColor Background color for all screens
///
/// \param font Font for text
///
/// \param boldFont Font for bold text
///
/// \param identificationInstructionImageForSelfie Custom image for Selfie Authentication instructions. We recommend using images smaller than 300dp
///
/// \param identificationInstructionImageForPassport Custom image for passport scan instructions. We recommend using images smaller than 300dp
///
/// \param identificationInstructionImageForDriverLicense Custom image for driver’s license scan instructions. We recommend using images smaller than 300dp
///
/// \param identificationInstructionImageForIdCard Custom image for ID card scan instructions. We recommend using images smaller than 300dp
///
- (instancetype)initWithPrimaryColor:(UIColor *)primaryColor
                           textColor:(UIColor *)textColor
               screenBackgroundColor:(UIColor *)screenBackgroundColor
                                font:(UIFont *)font boldFont:(UIFont *)boldFont
identificationInstructionImageForSelfie:(UIImage * _Nullable)identificationInstructionImageForSelfie
identificationInstructionImageForPassport:(UIImage * _Nullable)identificationInstructionImageForPassport
identificationInstructionImageForDriverLicense:(UIImage * _Nullable)identificationInstructionImageForDriverLicense
identificationInstructionImageForIdCard:(UIImage * _Nullable)identificationInstructionImageForIdCard __deprecated;

/// Theme class for UI customization
/// \param primaryColor Main color of your scheme
///
/// \param textColor Text color
///
/// \param screenBackgroundColor Background color for all screens
///
/// \param viewBackgroundHightlightColor Background highlight color
///
/// \param separatorColor Color for separator table lines
///
/// \param font Font for text
///
/// \param boldFont Font for bold text
///
/// \param identificationInstructionImageForSelfie Custom image for Selfie Authentication instructions. We recommend using images smaller than 300dp
///
/// \param identificationInstructionImageForPassport Custom image for passport scan instructions. We recommend using images smaller than 300dp
///
/// \param identificationInstructionImageForDriverLicense Custom image for driver’s license scan instructions. We recommend using images smaller than 300dp
///
/// \param identificationInstructionImageForIdCard Custom image for ID card scan instructions. We recommend using images smaller than 300dp
///
- (instancetype)initWithPrimaryColor:(UIColor *)primaryColor
                           textColor:(UIColor *)textColor
               screenBackgroundColor:(UIColor *)screenBackgroundColor
       viewBackgroundHightlightColor:(UIColor *)viewBackgroundHightlightColor
                      separatorColor:(UIColor *)separatorColor
                                font:(UIFont *)font
                            boldFont:(UIFont *)boldFont
identificationInstructionImageForSelfie:(UIImage * _Nullable)identificationInstructionImageForSelfie
identificationInstructionImageForPassport:(UIImage * _Nullable)identificationInstructionImageForPassport
identificationInstructionImageForDriverLicense:(UIImage * _Nullable)identificationInstructionImageForDriverLicense
identificationInstructionImageForIdCard:(UIImage * _Nullable)identificationInstructionImageForIdCard;

-(instancetype) init __attribute__((unavailable("init not available")));
@end

NS_ASSUME_NONNULL_END
