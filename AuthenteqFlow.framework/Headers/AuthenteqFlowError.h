//
//  AuthenteqFlowError.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>

/// Authenteq SDK Errors
typedef NS_ENUM(NSInteger, AuthenteqFlowError) {
  /// Identification or Selfie Authentication cancelled by user
  AuthenteqFlowErrorCancelledByUser = 0,
  /// SDK is running on jailbroken device
  AuthenteqFlowErrorJailbreakDetected = 1,
  /// Incorrect Authenteq license. Re-check that you are using correct license value for correct project bundle ID.
  /// If license and bundle ID is correct, contact to our support
  AuthenteqFlowErrorAuthenteqLicenseError = 2,
  /// Internal problem with liveness setup. Contact our support.
  AuthenteqFlowErrorLivenessSetupError = 3,
  /// Internal problem with ID document scan setup. Contact our support.
  AuthenteqFlowErrorOcrSetupError = 4,
  /// Internal problem with SDK setup. Contact our support.
  AuthenteqFlowErrorInternalSetupError = 5,
  /// Problem with establishing secure connection with our server. Check proxy/VPN settings for device
  AuthenteqFlowErrorConnectionCertificateError = 6,
  /// User account was deactivated for security reasons.
  AuthenteqFlowErrorUserDeactivated = 7,
  /// Incorrect <code>userId</code> provided in Selfie Authentication
  AuthenteqFlowErrorUserNotFound = 8,
  /// You started identification process with invalid configuration for ID documents scan steps
  AuthenteqFlowErrorInvalidConfiguration = 9,
};
