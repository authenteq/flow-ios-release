//
//  DocumentIdentificationResult.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DocumentIdentificationResult : NSObject
/// Id document type. Possible values:
/// <ul>
///   <li>
///     <code>PP</code> - passport
///   </li>
///   <li>
///     <code>NID</code> - national ID card
///   </li>
///   <li>
///     <code>DL</code> - driver’s license
///   </li>
/// </ul>
@property (nonatomic, readonly, strong) NSString * _Nullable documentType;

- (instancetype)initWithDocumentType:(NSString * _Nullable)documentType;

@end
NS_ASSUME_NONNULL_END
