//
//  AuthenteqFlowPublic.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class AuthenteqFlowTheme;
@class AuthenteqIdDocumentType;
@protocol AuthenteqIdentificationDelegate;
@protocol AuthenteqSelfieAuthentificationDelegate;

/// Main Class to access Authenteq SDK identification functionality
@interface AuthenteqFlow : NSObject

/// Theme property for customizable UI
@property (nonatomic, strong) AuthenteqFlowTheme * theme;

/// Singleton instance for Authenteq Flow SDK functionalities
@property (nonatomic, class, readonly, strong) AuthenteqFlow *instance;

- (UIViewController *)identificationViewControllerWith:(NSString *)license
                                             documents:(NSArray<NSArray<AuthenteqIdDocumentType *> *> *)documents
                                              delegate:(id <AuthenteqIdentificationDelegate>)delegate;

- (UIViewController *)selfieAuthenticationViewControllerWith:(NSString *)license
                                                      userId:(NSString *)userId
                                                    delegate:(id <AuthenteqSelfieAuthentificationDelegate>)delegate;

-(instancetype) init __attribute__((unavailable("init not available")));
@end

NS_ASSUME_NONNULL_END
