//
//  IdentificationResult.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentIdentificationResult.h"

NS_ASSUME_NONNULL_BEGIN

/// The class <code>IdentificationResult</code> will be returned after a successful identification
@interface IdentificationResult : NSObject

/// Authenteq user Id (to be used for future authentication flows)
@property (nonatomic, readonly, strong) NSString * userId;

/// Array with details of the identification documents
@property (nonatomic, readonly, copy) NSArray<DocumentIdentificationResult *> * documents;

/// File path to selfie image
@property (nonatomic, readonly, strong) NSString * _Nullable selfieImageFilePath;

/// String with JSON result for AML (Anti Money Laundering) check
@property (nonatomic, readonly, strong) NSString * _Nullable aml;

/// Recovery words for person’s account recovery
@property (nonatomic, readonly, strong) NSString * _Nullable recoveryWords __deprecated;

- (instancetype)initWithUserId:(NSString *)userId
                           aml:(NSString * _Nullable)aml
                     documents:(NSArray<DocumentIdentificationResult *> *)documents
           selfieImageFilePath:(NSString * _Nullable)selfieImageFilePath;
@end

NS_ASSUME_NONNULL_END
