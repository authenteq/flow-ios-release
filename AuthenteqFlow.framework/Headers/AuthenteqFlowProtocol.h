//
//  AuthenteqFlowProtocol.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthenteqFlowError.h"
NS_ASSUME_NONNULL_BEGIN

@class IdentificationResult;

@protocol AuthenteqIdentificationDelegate
/// Delegate callback on successfull completed identification
/// \param result identification data
///
- (void)authenteqDidFinishIdentificationWith:(IdentificationResult *)result;
/// Delegate callback on fail identification
/// \param error error details
///
- (void)authenteqDidFailIdentificationWith:(enum AuthenteqFlowError)error;
@end


/// Delegate protocol for selfie authentication
@protocol AuthenteqSelfieAuthentificationDelegate
/// Delegate callback on completed selfie authentication
/// \param result successfull value
///
- (void)authenteqDidFinishSelfieAuthenticationWith:(BOOL)result;
/// Delegate callback on fail identic
/// \param error error details
///
- (void)authenteqDidFailSelfieAuthenticationWith:(enum AuthenteqFlowError)error;
@end

NS_ASSUME_NONNULL_END
