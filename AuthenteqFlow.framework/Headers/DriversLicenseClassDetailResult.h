//
//  DriversLicenseClassDetailResult.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// Additional Detail for the Driver’s License Class
@interface DriversLicenseClassDetailResult : NSObject

/// Class validity start date
@property (nonatomic, readonly, strong) NSDate * _Nullable from;

/// Class validity end date
@property (nonatomic, readonly, strong) NSDate * _Nullable to;

/// Class additional notes
@property (nonatomic, readonly, strong) NSString * _Nullable notes;

- (instancetype)initWithFrom:(NSDate * _Nullable)from
                          to:(NSDate * _Nullable)to
                       notes:(NSString * _Nullable)notes;
@end


NS_ASSUME_NONNULL_END
