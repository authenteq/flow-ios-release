//
//  PassportIdentificationResult.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentIdentificationResult.h"

NS_ASSUME_NONNULL_BEGIN

/// Identification details in case of Passport ID
@interface PassportIdentificationResult : DocumentIdentificationResult

/// Given names (separated with whitespace)
@property (nonatomic, readonly, strong) NSString * _Nullable givenNames;

/// Surnames (separated with whitespace)
@property (nonatomic, readonly, strong) NSString * _Nullable surname;

/// Date of birth
@property (nonatomic, readonly, strong) NSDate * _Nullable dateOfBirth;

/// Date of expiration of the document
@property (nonatomic, readonly, strong) NSDate * _Nullable dateOfExpiry;

/// Date of issue of the document
@property (nonatomic, readonly, strong) NSDate * _Nullable dateOfIssue;

/// Possible values are:
/// <ul>
///   <li>
///     <code>M</code> (male)
///   </li>
///   <li>
///     <code>F</code> (female)
///   </li>
///   <li>
///     <code>X</code> (unspecified)
///   </li>
/// </ul>
@property (nonatomic, readonly, strong) NSString * _Nullable sex;

/// Country code of user’s nationality in ISO 3166-1 alpha-3 format
@property (nonatomic, readonly, strong) NSString * _Nullable nationality;

/// Country code of document’s issuing country in ISO 3166-1 alpha-3 format
@property (nonatomic, readonly, strong) NSString * _Nullable issuingCountry;

/// ID document number
@property (nonatomic, readonly, strong) NSString * _Nullable documentNumber;

/// Is person’s age over 16 years. Possible values:
/// <ul>
///   <li>
///     <code>0</code> - false
///   </li>
///   <li>
///     <code>1</code> - true
///   </li>
/// </ul>
@property (nonatomic, readonly, strong) NSNumber * _Nullable isSixteenPlus;

/// Is person’s age over 18 years. Possible values:
/// <ul>
///   <li>
///     <code>0</code> - false
///   </li>
///   <li>
///     <code>1</code> - true
///   </li>
/// </ul>
@property (nonatomic, readonly, strong) NSNumber * _Nullable isEighteenPlus;

/// Is person’s age over 21 years. Possible values:
/// <ul>
///   <li>
///     <code>0</code> - false
///   </li>
///   <li>
///     <code>1</code> - true
///   </li>
/// </ul>
@property (nonatomic, readonly, strong) NSNumber * _Nullable isTwentyOnePlus;

/// File path to the image of passport front page
@property (nonatomic, readonly, strong) NSString * _Nullable documentImageFilePath;

- (instancetype)initWithGivenNames:(NSString * _Nullable)givenNames
                           surname:(NSString * _Nullable)surname
                       dateOfBirth:(NSDate * _Nullable)dateOfBirth
                      dateOfExpiry:(NSDate * _Nullable)dateOfExpiry
                       dateOfIssue:(NSDate * _Nullable)dateOfIssue
                               sex:(NSString * _Nullable)sex
                       nationality:(NSString * _Nullable)nationality
                    issuingCountry:(NSString * _Nullable)issuingCountry
                    documentNumber:(NSString * _Nullable)documentNumber
                     isSixteenPlus:(NSNumber * _Nullable)isSixteenPlus
                    isEighteenPlus:(NSNumber * _Nullable)isEighteenPlus
                   isTwentyOnePlus:(NSNumber * _Nullable)isTwentyOnePlus
             documentImageFilePath:(NSString * _Nullable)documentImageFilePath;
@end

NS_ASSUME_NONNULL_END
