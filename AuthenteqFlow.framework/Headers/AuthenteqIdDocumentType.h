//
//  AuthenteqIdDocumentType.h
//  AuthenteqFlow
//
//  Created by Vitalii Gozhenko on 27.03.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// Class with ID Document types supported
@interface AuthenteqIdDocumentType : NSObject

/// Passport type of document
@property (nonatomic, class, readonly, strong) AuthenteqIdDocumentType *passport;

/// Driver’s License type of document
@property (nonatomic, class, readonly, strong) AuthenteqIdDocumentType *driversLicense;

/// National ID card type of document
@property (nonatomic, class, readonly, strong) AuthenteqIdDocumentType *idCard;

-(instancetype) init __attribute__((unavailable("init not available")));

@end

NS_ASSUME_NONNULL_END
